# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.
# All rights reserved.
#
#	This source code contains the intellectual property
#	of its copyright holder(s), and is made available
#	under a license.  If you do not know the terms of
#	the license, please stop and do not read further.
from _yottadb import ffi, lib
import ctypes

data = lib.ydb_data_st
delete = lib.ydb_delete_st
delete_excl = lib.ydb_delete_excl_st
get = lib.ydb_get_st
incr = lib.ydb_incr_st
node_next = lib.ydb_node_next_st
node_prev = lib.ydb_node_previous_st
set = lib.ydb_set_st
sub_next = lib.ydb_subscript_next_st
sub_prev = lib.ydb_subscript_previous_st

YDB_DEL_TREE = 1
YDB_DEL_NODE = 2

YDB_ERR_NODEEND = -151027922

class Buffer:
    def __init__(self, value=None, length=0, buf=None):
        """ Allocates a new buffer with the specified length, or if omitted,
        allocated a buffer large enough to store the value specified. If that
        is omitted, raises an exception
        """
        if buf != None:
            self.buf = buf
            self._buf_addr = self.buf.buf_addr
            return
        self.buf = ffi.new("ydb_buffer_t *")
        if length == 0:
            if value == None:
                raise "Error!"
            length = len(value)
        self._buf_addr = ffi.new("char[%d]" % length)
        self.buf.buf_addr = self._buf_addr
        self.buf.len_alloc = length
        self.buf.len_used = 0
        if value != None:
            self.set_val(value)

    def __repr__(self):
        return "len_alloc = %d, len_used = %d, buff = %s" % (self.buf.len_alloc, self.buf.len_used, ffi.string(self._buf_addr))
    def val(self):
        """ Returns the value of this buffer casted as a string;
        """
        return ffi.string(self._buf_addr)

    def set_val(self, value):
        """ Sets the value of this buffer to value; if the length of value
        is greater than the length allocated for this buffer, raises an exception
        """
        if self.buf.len_alloc < len(value):
            raise "Error!"
        try:
            value = value.encode('ascii')
        except AttributeError:
            pass
        for i, c in enumerate(value):
            self._buf_addr[i] = bytes([c])
        self.buf.len_used = len(value)

class BufferArray:
    def __init__(self, values=None, length=0, num_buffers=0):
        """ Constructs a new array of contiguous buffers
        Access these buffers by looking at the buffers element on
        this object
        status = sub_next(tptoken, err.buf, gbl.buf, 1, subs.buf, subs[0].buf)
        if status != 0:
            raise str(err.val())
        self.assertEqual(b'subA', subs[0].val())
        """
        if num_buffers == 0:
            if values == None:
                raise "Error!"
            num_buffers = len(values)
        if length == 0:
            if values == None:
                raise "Error!"
            length = len(values[0])
        # Allocate an array of buffer_t structs
        self.buffers = []
        self._buffers = ffi.new("ydb_buffer_t[%d]" % num_buffers)
        # Put the start of the _buffers array somewhere easy to access
        self.buf = self._buffers
        self._buff_addrs = []
        for i in range(0, num_buffers):
            self._buff_addrs += [ffi.new("char[%d]" % length)]
            self._buffers[i].buf_addr = self._buff_addrs[i]
            self._buffers[i].len_alloc = length
            self._buffers[i].len_used = 0
            self.buffers += [Buffer(buf=ffi.addressof(self._buffers[i]))]
            if values != None:
                for j, c in enumerate(values[i].encode('ascii')):
                    self._buff_addrs[i][j] = bytes([c])
                self._buffers[i].len_used = len(values[i])

    def __getitem__(self, index):
        return self.buffers[index]

class Context:
    def __init__(self):
        self.errstr = Buffer(length=1024)
        self.tptoken = 0

    def key(self, varname, *args):
        ret = Key(varname, context=self)
        for arg in args:
            ret = ret[arg]
        return ret

class ForwardKeySubIter:
    """ Represents a forward subscript-level iterator
    """
    def __init__(self, key):
        self.key = key
        self.root = key.root
        self.cur_buffer = key.cur_buffer
        self.key.root.buffers._buffers[key.cur_buffer+1].len_used = 0

    def __next__(self):
        ctx = self.root.context
        result = sub_next(ctx.tptoken, ctx.errstr.buf, self.root.buffers.buf, self.cur_buffer + 1,
                    ffi.addressof(self.root.buffers.buf, 1),
                    ffi.addressof(self.root.buffers.buf, self.cur_buffer + 1))
        buf = self.key.root.buffers[self.cur_buffer + 1].buf
        buf.buf_addr[buf.len_used] = b'\0'
        if result == YDB_ERR_NODEEND:
            raise StopIteration
        return self.key.root.buffers[self.cur_buffer + 1]

class ReverseKeySubIter:
    """ Represents a reverse subscript-level iterator
    """
    def __init__(self, key):
        self.key = key
        self.root = key.root
        self.cur_buffer = key.cur_buffer
        self.key.root.buffers._buffers[key.cur_buffer+1].len_used = 0

    def __next__(self):
        ctx = self.root.context
        result = sub_prev(ctx.tptoken, ctx.errstr.buf, self.root.buffers.buf, self.cur_buffer + 1,
                    ffi.addressof(self.root.buffers.buf, 1),
                    ffi.addressof(self.root.buffers.buf, self.cur_buffer + 1))
        buf = self.key.root.buffers[self.cur_buffer + 1].buf
        buf.buf_addr[buf.len_used] = b'\0'
        if result == YDB_ERR_NODEEND:
            raise StopIteration
        return self.key.root.buffers[self.cur_buffer + 1]

class Key:
    def __init__(self, varname, context=None, root=None, parent=None):
        """ Constructs a key with the given global node

        >>> myKey = Key("^hello")
        >>> myKey.get()
        b''
        >>> myKey["subA"].get()
        b'hello!'
        """
        if root != None:
            self.root = root
            if parent == None:
                raise "Error!"
            self.parent = parent
            self.cur_buffer = parent.cur_buffer + 1
        else:
            self.root = self
            self.parent = None
            self.cur_buffer = 0
            self.buffers = BufferArray(num_buffers=32, length=255)
            self.context = context
        self.root.buffers[self.cur_buffer].set_val(varname)

    def get(self):
        """ Fetches the value for this key from the database
        >>> ctx = Context()
        >>> k = ctx.key("^hello")
        >>> k.get()
        b'hello world!'
        """
        res = Buffer(length=1024)
        status = get(self.root.context.tptoken, self.root.context.errstr.buf,
                self.root.buffers.buf, self.cur_buffer,
                ffi.addressof(self.root.buffers.buf, 1), res.buf)
        if status != 0:
            raise Exception("Error: %s" % (self.root.context.errstr.val()))
        res.buf.buf_addr[res.buf.len_used] = b'\0'
        return res.val()

    def delete(self, deltype=YDB_DEL_TREE):
        """ Deletes a value from the database. Provide either YDB_DEL_TREE
        or YDB_DEL_NODE to specify delete type
        >>> ctx = Context()
        >>> k = ctx.key("^hello")
        >>> k.delete()
        """
        status = delete(self.root.context.tptoken, self.root.context.errstr.buf,
                self.root.buffers.buf, self.cur_buffer,
                ffi.addressof(self.root.buffers.buf, 1), int(deltype))
        if status != 0:
            raise Exception("Error: %s" % (self.root.context.errstr.val()))

    def data(self):
        """ Checks to see if the key exists in the database;
        return values are 0 (no data), 1 (node value), 10 (tree value), or
        11 (node-tree value)
        >>> ctx = Context()
        >>> k = ctx.key("^hello")
        >>> k.get()
        1
        """
        res = ffi.new("unsigned int *")
        status = data(self.root.context.tptoken, self.root.context.errstr.buf,
                self.root.buffers.buf, self.cur_buffer,
                ffi.addressof(self.root.buffers.buf, 1), res)
        if status != 0:
            raise Exception("Error: %s" % (self.root.context.errstr.val()))
        return ffi.unpack(res,1)[0]

    def set(self, value):
        """ Sets the value for this key from the database
        >>> ctx = Context()
        >>> k = ctx.key("^hello")
        >>> k.set('hello world!')
        """
        res = Buffer(value=str(value))
        status = set(self.root.context.tptoken, self.root.context.errstr.buf,
                self.root.buffers.buf, self.cur_buffer,
                ffi.addressof(self.root.buffers.buf, 1), res.buf)
        if status != 0:
            raise Exception("Error: %s" % (self.root.context.errstr.val()))

    def incr(self, amount=None):
        """ Increments the value of this key in the database and returns
        the new value

        >>> ctx = Context()
        >>> k = ctx.key("^hello")
        >>> k.incr()
        b'1'
        """
        res = Buffer(length=1024)
        amount_b = ffi.NULL
        if amount != None:
            amount_b = Buffer(amount)
        status = incr(self.root.context.tptoken, self.root.context.errstr.buf,
                self.root.buffers.buf, self.cur_buffer,
                ffi.addressof(self.root.buffers.buf, 1), amount_b, res.buf)
        if status != 0:
            raise Exception("Error: %s" % (self.root.context.errstr.val()))
        res.buf.buf_addr[res.buf.len_used] = b'\0'
        return res.val()

    def set_val(self, value):
        return self.root.buffers[self.cur_buffer].set_val(value)

    def val(self):
        return self.root.buffers[self.cur_buffer].val()

    def __repr__(self):
        res = []
        for b in self.root.buffers:
            res += [b.__repr__()]
        return '\n'.join(res)

    def __getitem__(self, subscript):
        return Key(subscript, root=self.root, parent=self)

    def __iter__(self):
        return ForwardKeySubIter(self)

    def __reversed__(self):
        class t:
            def __init__(self, key):
                self.key = key
            def __iter__(self):
                return ReverseKeySubIter(self.key)
        return t(self)

def tp(tptoken, errstr, callback, transid="BATCH", varnames=None):
    """ callback must return int, and take callback(tptoken, errstr)
    """
    ptr = ffi.new_handle(callback)
    transid = ffi.new("char[]", transid.encode("ascii"))
    return lib.ydb_tp_st(tptoken, errstr, lib.tpCallback, ptr, transid, 0, ffi.NULL)

@ffi.def_extern()
def tpCallback(tptoken, errstr, parm):
    ptr = ffi.from_handle(parm)
    ptr(tptoken, errstr)
    return 0
