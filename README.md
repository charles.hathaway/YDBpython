# YottaDB Python Wrapper

This is a work in progress.

## Step 0 - Install YottaDB

See instructions at https://yottadb.com/product/get-started/

## Step 1 - Install YottaDB Wrapper

```
pip install git+https://gitlab.com/charles.hathaway/YDBpython.git
```

## Step 2 - Use YottaDB

```
from yottadb import *

gbl = Buffer("^hello")
err = Buffer(length=1024)
val = Buffer("World!")
tptoken = 0

status = set(tptoken, err, gbl.buf, 0, ffi.NULL, val.buf)
if status != 0:
  raise err.val()

print("Value set in database!")
```
