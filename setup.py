from setuptools import setup

setup(name="YottaDB",
        version="0.0.1",
        description="Wrappers for the YottaDB database",
        author="Charles Hathaway",
        author_email="charles@yottadb.com",
        setup_requires=["cffi>=1.0.0"],
        cffi_modules=["yottadb_build.py:ffibuilder"],
        install_requires=["cffi>=1.0.0"],
        py_modules=["yottadb"],
        test_suite="tests"
        )
